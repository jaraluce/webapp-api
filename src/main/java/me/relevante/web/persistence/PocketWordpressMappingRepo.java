package me.relevante.web.persistence;

import me.relevante.model.PocketWordpressMapping;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by daniel-ibanez on 26/08/16.
 */
@Repository
public interface PocketWordpressMappingRepo extends MongoRepository<PocketWordpressMapping, String> {
    List<PocketWordpressMapping> findByRelevanteIdAndWpHubId(String relevanteId, String hubId);
}
