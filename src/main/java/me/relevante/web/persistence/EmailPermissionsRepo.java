package me.relevante.web.persistence;

import me.relevante.web.model.EmailPermissions;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailPermissionsRepo extends MongoRepository<EmailPermissions, ObjectId> {
    EmailPermissions findOneByEmail(String email);
}