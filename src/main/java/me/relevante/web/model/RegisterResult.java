package me.relevante.web.model;

public class RegisterResult {

    private String relevanteId;
    private boolean isNewUser;
    private String authToken;

    public RegisterResult(String relevanteId,
                          boolean isNewUser,
                          String authToken) {
        this.relevanteId = relevanteId;
        this.isNewUser = isNewUser;
        this.authToken = authToken;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public boolean isNewUser() {
        return isNewUser;
    }

    public String getAuthToken() {
        return authToken;
    }
}
