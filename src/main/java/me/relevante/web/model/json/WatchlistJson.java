package me.relevante.web.model.json;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daniel-ibanez
 */
public class WatchlistJson {

    private String name;
    private List<String> searchTerms;
    private String watchlistId;
    private Long num;

    public WatchlistJson() {
        this.searchTerms = new ArrayList<>();
    }

    public String getWatchlistId() {
        return watchlistId;
    }

    public void setWatchlistId(String watchlistId) {
        this.watchlistId = watchlistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSearchTerms() {
        return searchTerms;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }
}
