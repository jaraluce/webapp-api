package me.relevante.web.model.json;

import me.relevante.network.Network;

public class OAuthAccessRequest<N extends Network> {

    protected String requestToken;
    protected String requestSecret;
    protected String oAuthUrl;

    public OAuthAccessRequest(String requestToken, String requestSecret, String oAuthUrl) {
        this.requestToken = requestToken;
        this.requestSecret = requestSecret;
        this.oAuthUrl = oAuthUrl;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public String getRequestSecret() {
        return requestSecret;
    }

    public String getOAuthUrl() {
        return oAuthUrl;
    }
}
