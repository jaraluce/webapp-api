package me.relevante.web.model.json;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 27/09/16.
 */
public class TagUsersJson {

    private String tag;
    private List<String> encodedUserIds;

    public TagUsersJson() {
        this.encodedUserIds = new ArrayList<>();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<String> getEncodedUserIds() {
        return encodedUserIds;
    }

}
