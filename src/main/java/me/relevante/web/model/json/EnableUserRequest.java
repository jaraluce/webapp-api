package me.relevante.web.model.json;

import java.util.List;

/**
 * @author daniel-ibanez
 */
public class EnableUserRequest {

    private List<String> emails;
    private String hubUrl;
    private String username;
    private String password;
    private boolean bulkEngageEnabled;

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public String getHubUrl() {
        return hubUrl;
    }

    public void setHubUrl(String hubUrl) {
        this.hubUrl = hubUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBulkEngageEnabled() {
        return bulkEngageEnabled;
    }

    public void setBulkEngageEnabled(boolean bulkEngageEnabled) {
        this.bulkEngageEnabled = bulkEngageEnabled;
    }
}
