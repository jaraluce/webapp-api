package me.relevante.web.model.json;

/**
 * Created by daniel-ibanez on 26/08/16.
 */
public class PocketWordpressMappingJson {

    private String id;
    private String pocket;
    private String match;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPocket() {
        return pocket;
    }

    public void setPocket(String pocket) {
        this.pocket = pocket;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }
}
