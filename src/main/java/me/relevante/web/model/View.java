package me.relevante.web.model;

public class View {
    private static final String PAGES_PREFIX = "/static/templates/pages/";
    public static final String APP_CRASH = PAGES_PREFIX + "appCrash.html";
}
