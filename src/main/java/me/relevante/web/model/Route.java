package me.relevante.web.model;

public class Route {

    public static final String NETWORK = "/{networkName}";

    public static final String API_ACCOUNT = CoreRoute.API_PREFIX + "/account";
    public static final String API_ACTIONS = CoreRoute.API_PREFIX + "/actions";
    public static final String API_ADMIN = CoreRoute.API_PREFIX + "/admin";
    public static final String API_AUTH = CoreRoute.API_PREFIX + "/auth";
    public static final String API_BLACKLIST = CoreRoute.API_PREFIX + "/blacklist";
    public static final String API_CONNECT_NETWORK = CoreRoute.API_PREFIX + NETWORK + "/connect";
    public static final String API_DISCONNECT_NETWORK = CoreRoute.API_PREFIX + NETWORK + "/disconnect";
    public static final String API_PENDING_ACTIONS = CoreRoute.API_PREFIX + "/actions/pending";
    public static final String API_POCKET_WORDPRESS_HUB_MAPPINGS = WordpressRoute.API_HUBS + "/{hubId}/mappings";
    public static final String API_REGISTER = CoreRoute.API_PREFIX + "/register";
    public static final String API_TAGS = CoreRoute.API_PREFIX + "/tags";
    public static final String API_USERS = CoreRoute.API_PREFIX + "/users";
    public static final String API_WATCHLIST_USERS_BY_WATCHLIST = CoreRoute.API_PREFIX + "/watchlists/{watchlistId}/users";
    public static final String API_WATCHLISTS = CoreRoute.API_PREFIX + "/watchlists";

}
