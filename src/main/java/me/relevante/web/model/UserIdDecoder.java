package me.relevante.web.model;

import me.relevante.model.NetworkUser;
import me.relevante.network.Network;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.List;

/**
 * Created by daniel-ibanez on 11/09/16.
 */
@Component
public class UserIdDecoder {

    private List<Network> networks;

    @Autowired
    public UserIdDecoder(List<Network> networks) {
        this.networks = networks;
    }

    public NetworkUser decode(String encodedUserId) {
        String[] decodedUserItems = new String(Base64.getDecoder().decode(encodedUserId)).split(" ");
        String networkName = decodedUserItems[0];
        final Network network = networks.stream().filter(n -> n.getName().equals(networkName)).findFirst().get();
        final String userId = decodedUserItems[1];
        NetworkUser id = new NetworkUser() {
            @Override
            public String getUserId() {
                return userId;
            }

            @Override
            public Network getNetwork() {
                return network;
            }
        };
        return id;
    }
}
