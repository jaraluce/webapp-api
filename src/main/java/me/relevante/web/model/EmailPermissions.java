package me.relevante.web.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "EmailPermissions")
public class EmailPermissions {

    @Id
    private String email;
    private Boolean allowedToLogin;
    private Boolean allowedToDelete;
    private Boolean admin;

    public EmailPermissions(String email,
                            boolean allowedToLogin,
                            boolean allowedToDelete,
                            boolean admin) {
        this.email = email;
        this.allowedToLogin = allowedToLogin;
        this.allowedToDelete = allowedToDelete;
        this.admin = admin;
    }

    private EmailPermissions() {
    }

    public String getEmail() {
        return email;
    }

    public Boolean isAllowedToLogin() {
        return allowedToLogin;
    }

    public void setAllowedToLogin(boolean allowedToLogin) {
        this.allowedToLogin = allowedToLogin;
    }

    public Boolean isAllowedToDelete() {
        return allowedToDelete;
    }

    public Boolean isAdmin() {
        return admin;
    }
}
