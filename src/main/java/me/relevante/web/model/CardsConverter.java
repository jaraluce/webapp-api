package me.relevante.web.model;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkSearchUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.web.model.card.NetworkFullCardMapper;
import me.relevante.web.model.card.NetworkSearchCardMapper;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.model.utils.FindByNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CardsConverter {

    private static final Logger logger = LoggerFactory.getLogger(CardsConverter.class);

    private List<NetworkFullCardMapper> networkFullCardMappers;
    private List<NetworkSearchCardMapper> networkSearchCardMappers;

    @Autowired
    public CardsConverter(List<NetworkFullCardMapper> networkFullCardMappers, List<NetworkSearchCardMapper> networkSearchCardMappers) {
        this.networkFullCardMappers = networkFullCardMappers;
        this.networkSearchCardMappers = networkSearchCardMappers;
    }

    public List<NetworkCardUser> convertFullUsers(List<NetworkFullUser> fullUsers,
                                                  RelevanteAccount relevanteAccount,
                                                  RelevanteContext relevanteContext) {
        List<NetworkCardUser> cardUsers = new ArrayList<>();
        for (NetworkFullUser fullUser : fullUsers) {
            NetworkFullCardMapper cardMapper = new FindByNetwork<>(networkFullCardMappers).find(fullUser.getNetwork());
            NetworkCardUser cardUser = cardMapper.mapFullUser(fullUser, relevanteAccount, relevanteContext);
            cardUsers.add(cardUser);
        }
        return cardUsers;
    }

    public List<NetworkCardUser> convertSearchUsers(List<NetworkSearchUser> searchUsers,
                                                    RelevanteAccount relevanteAccount,
                                                    RelevanteContext relevanteContext) {
        List<NetworkCardUser> cardUsers = new ArrayList<>();
        for (NetworkSearchUser searchUser : searchUsers) {
            NetworkSearchCardMapper cardMapper = new FindByNetwork<>(networkSearchCardMappers).find(searchUser.getNetwork());
            // FIXME
            try {
                NetworkCardUser cardUser = cardMapper.mapSearchUser(searchUser, relevanteAccount, relevanteContext);
                cardUsers.add(cardUser);
            } catch (RuntimeException e) {
                logger.error("Error mapping search user " + searchUser.getId(), e);
            }
        }
        return cardUsers;
    }

}
