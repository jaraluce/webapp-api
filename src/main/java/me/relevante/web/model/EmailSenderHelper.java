package me.relevante.web.model;

import me.relevante.utils.message.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 30/09/16.
 */
@Component
public class EmailSenderHelper {

    private String alertsRecipient;
    private EmailSender emailSender;

    @Autowired
    public EmailSenderHelper(EmailSender emailSender,
                             @Value("${custom.alerts.recipient}") String alertsRecipient) {
        this.emailSender = emailSender;
        this.alertsRecipient = alertsRecipient;
    }

    public void sendAlert(String subject, String message) throws Exception {
        emailSender.sendMessage(alertsRecipient, subject, message);
    }
}
