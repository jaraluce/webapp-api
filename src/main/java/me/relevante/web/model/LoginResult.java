package me.relevante.web.model;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginResult {

    private RelevanteAccount relevanteAccount;
    private RelevanteContext relevanteContext;
    private Map<Network, NetworkFullUser> fullUsers;
    private boolean isAllowedToLogin;

    public LoginResult(RelevanteAccount relevanteAccount,
                       RelevanteContext relevanteContext,
                       boolean isAllowedToLogin,
                       List<NetworkFullUser> fullUsers) {
        this.relevanteAccount = relevanteAccount;
        this.relevanteContext = relevanteContext;
        this.isAllowedToLogin = isAllowedToLogin;
        this.fullUsers = new HashMap<>();
        fullUsers.forEach(networkFullUser -> this.fullUsers.put(networkFullUser.getNetwork(), networkFullUser));
    }

    public RelevanteAccount getRelevanteAccount() {
        return relevanteAccount;
    }

    public RelevanteContext getRelevanteContext() {
        return relevanteContext;
    }

    public boolean isAllowedToLogin() {
        return isAllowedToLogin;
    }

    public NetworkFullUser getNetworkFullUser(Network network) {
        return this.fullUsers.get(network);
    }


}
