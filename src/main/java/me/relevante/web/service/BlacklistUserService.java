package me.relevante.web.service;

import me.relevante.model.BlacklistUser;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.utils.ListsMerger;
import me.relevante.web.model.UserIdDecoder;
import me.relevante.web.model.CardsConverter;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BlacklistUserService {

    private RelevanteAccountRepo relevanteAccountRepo;
    private RelevanteContextRepo relevanteContextRepo;
    private List<NetworkBlacklistUserService> networkBlacklistUserServices;
    private ListsMerger<NetworkFullUser> listsMerger;
    private CardsConverter cardsConverter;
    private UserIdDecoder userDecoder;

    @Autowired
    public BlacklistUserService(RelevanteAccountRepo relevanteAccountRepo,
                                RelevanteContextRepo relevanteContextRepo,
                                List<NetworkBlacklistUserService> networkBlacklistUserServices,
                                ListsMerger<NetworkFullUser> listsMerger,
                                UserIdDecoder userDecoder,
                                CardsConverter cardsConverter) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.networkBlacklistUserServices = new ArrayList<>(networkBlacklistUserServices);
        this.listsMerger = listsMerger;
        this.userDecoder = userDecoder;
        this.cardsConverter = cardsConverter;
    }

    public List<NetworkCardUser> getBlacklistUsers(String relevanteId,
                                           Integer maxElements) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);

        List<NetworkFullUser> blacklistUsers = createBlacklistUsersList(relevanteAccount, relevanteContext);

        // Do pagination
        maxElements = (maxElements == null) ? Integer.MAX_VALUE : maxElements;
        int endIndex = Math.min(maxElements, blacklistUsers.size());
        List<NetworkFullUser> paginatedUsers = new ArrayList<>(blacklistUsers.subList(0, endIndex));

        List<NetworkCardUser> cardUsers = cardsConverter.convertFullUsers(paginatedUsers, relevanteAccount, relevanteContext);

        return cardUsers;
    }


    List<NetworkFullUser> createBlacklistUsersList(RelevanteAccount relevanteAccount,
                                                   RelevanteContext relevanteContext) {

        List<List<? extends NetworkFullUser>> networkFullUsers = new ArrayList<>();
        for (NetworkBlacklistUserService networkBlacklistUserService : networkBlacklistUserServices) {
            List<NetworkFullUser> blacklistUsers = networkBlacklistUserService.getNetworkBlacklistUsers(relevanteAccount, relevanteContext);
            networkFullUsers.add(blacklistUsers);
        }
        List<NetworkFullUser> allUsers = listsMerger.mergeAlternatingElements(networkFullUsers);

        return allUsers;
    }

    public void addUserToBlacklist(String relevanteId,
                                   String encodedUserId) {

        NetworkUser networkUser = userDecoder.decode(encodedUserId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);

        BlacklistUser blacklistUser = new BlacklistUser(relevanteContext.getRelevanteId(), networkUser.getNetwork(), networkUser.getUserId());
        if (relevanteContext.addBlacklistUser(blacklistUser) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
    }

    public void removeUserFromBlacklist(String relevanteId,
                                        String encodedUserId) {

        NetworkUser networkUser = userDecoder.decode(encodedUserId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);

        BlacklistUser blacklistUser = new BlacklistUser(relevanteContext.getRelevanteId(), networkUser.getNetwork(), networkUser.getUserId());
        if (relevanteContext.removeBlacklistUser(blacklistUser) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
    }

}
