package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.model.json.LoginRequest;
import me.relevante.web.model.json.RegistrationRequest;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.Date;
import java.util.UUID;

@Service
public class RegisterService {

    private static final long MINUTE_IN_SECONDS = 60;
    private static final long HOUR_IN_SECONDS = 60 * MINUTE_IN_SECONDS;
    private static final long DAY_IN_SECONDS = 24 * HOUR_IN_SECONDS;
    private static final long MIN_TIME_BETWEEN_ACTIONS_IN_SECONDS = 30;
    private static final long MAX_TIME_WITHOUT_MONITORING_IN_SECONDS = 2 * DAY_IN_SECONDS;
    private static final long DEFAULT_MAX_MONITORED_USERS = 1000;
    private static final String DEFAULT_ACTIONS_TIME_RANGE = "08:00-22:00";

    private final RelevanteAccountRepo relevanteAccountRepo;
    private final RelevanteContextRepo relevanteContextRepo;
    private final AuthService authService;

    @Autowired
    public RegisterService(RelevanteAccountRepo relevanteAccountRepo,
                           RelevanteContextRepo relevanteContextRepo,
                           AuthService authService) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.authService = authService;
    }

    public RegisterResult registerAccount(RegistrationRequest registrationRequest) throws AuthenticationException {

        if (registrationRequest == null) {
            throw new IllegalArgumentException();
        }
        if (StringUtils.isBlank(registrationRequest.getEmail()) || StringUtils.isBlank(registrationRequest.getPassword())) {
            throw new IllegalArgumentException();
        }

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOneByEmail(registrationRequest.getEmail());
        if (relevanteAccount != null) {
            RegisterResult registerResult = new RegisterResult(relevanteAccount.getId(), false, null);
            return registerResult;
        }

        relevanteAccount = new RelevanteAccount();
        relevanteAccount.setId(UUID.randomUUID().toString());
        relevanteAccount.setName(registrationRequest.getName());
        relevanteAccount.setEmail(registrationRequest.getEmail());
        relevanteAccount.setPassword(registrationRequest.getPassword());
        relevanteAccount.setCreationTimestamp(new Date());
        relevanteAccount.setLastAccessTimestamp(new Date());
        relevanteAccount.setMaxMonitoredUsers(DEFAULT_MAX_MONITORED_USERS);
        relevanteAccount.setMinTimeBetweenActionsInSeconds(MIN_TIME_BETWEEN_ACTIONS_IN_SECONDS);
        relevanteAccount.setMaxTimeWithoutMonitoringInSeconds(MAX_TIME_WITHOUT_MONITORING_IN_SECONDS);
        relevanteAccount.getActionsTimeRanges().add(DEFAULT_ACTIONS_TIME_RANGE);
        RelevanteContext relevanteContext = new RelevanteContext(relevanteAccount.getId());
        relevanteContextRepo.save(relevanteContext);
        relevanteAccountRepo.save(relevanteAccount);

        String authToken = authService.processAuth(new LoginRequest(registrationRequest.getEmail(), registrationRequest.getPassword()));

        RegisterResult registerResult = new RegisterResult(relevanteAccount.getId(), true, authToken);
        return registerResult;
    }

}
