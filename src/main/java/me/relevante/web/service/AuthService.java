package me.relevante.web.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import me.relevante.model.RelevanteAccount;
import me.relevante.web.model.json.LoginRequest;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * Created by daniel-ibanez on 11/09/16.
 */
@Service
public class AuthService {

    private String secretKey;
    private RelevanteAccountRepo accountRepo;
    private EmailPermissionsService emailPermissionsService;

    @Autowired
    public AuthService(RelevanteAccountRepo accountRepo,
                       EmailPermissionsService emailPermissionsService,
                       @Value("${auth.jwtSecretKey}") String secretKey) {
        this.accountRepo = accountRepo;
        this.emailPermissionsService = emailPermissionsService;
        this.secretKey = secretKey;
    }

    public String processAuth(LoginRequest login) throws AuthenticationException {

        RelevanteAccount relevanteAccount = accountRepo.findOneByEmailAndPassword(login.getEmail(), login.getPassword());
        if (relevanteAccount == null) {
            throw new AuthenticationException();
        }
        if (!emailPermissionsService.isAllowedToLogin(relevanteAccount.getId())) {
            throw new AuthenticationException();
        }
        LocalDateTime expirationLocalDateTime = LocalDateTime.now(ZoneOffset.UTC).plusDays(1);
        Date expirationDate = Date.from(expirationLocalDateTime.toInstant(ZoneOffset.UTC));
        String jwtToken = Jwts.builder()
                .setSubject(relevanteAccount.getId())
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
        return jwtToken;
    }

}
