package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.network.Network;
import me.relevante.web.model.json.Level1ActionJson;
import me.relevante.web.model.json.Level2ActionJson;
import me.relevante.web.model.json.Level3ActionJson;
import me.relevante.web.model.json.Level4ActionJson;
import me.relevante.web.model.utils.FindByNetwork;
import me.relevante.web.model.utils.FindByNetworkName;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActionsService {

    private final RelevanteAccountRepo relevanteAccountRepo;
    private final List<Network> networks;
    private final List<NetworkActionService> networkActionServices;

    @Autowired
    public ActionsService(RelevanteAccountRepo relevanteAccountRepo,
                          List<Network> networks,
                          List<NetworkActionService> networkActionServices) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.networks = new ArrayList<>(networks);
        this.networkActionServices = new ArrayList<>(networkActionServices);
    }

    public List<String> processLevel1Actions(String relevanteId,
                                             List<Level1ActionJson> actions) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        List<String> actionsResponses = new ArrayList<>();
        for (Level1ActionJson action : actions) {
            String actionResponseText = processLevel1Action(relevanteAccount, action);
            actionsResponses.add(actionResponseText);
        }
        return actionsResponses;
    }

    public String processLevel1Action(RelevanteAccount relevanteAccount,
                                      Level1ActionJson action) {

        Network network = new FindByNetworkName(networks).find(action.getNetwork());
        NetworkActionService actionService = new FindByNetwork<>(networkActionServices).find(network);
        String actionResponseText = actionService.putLevel1Action(relevanteAccount.getId(), action.getPostId());
        return actionResponseText;
    }

    public List<String> processLevel2Actions(String relevanteId,
                                             List<Level2ActionJson> actions) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        List<String> actionsResponses = new ArrayList<>();
        for (Level2ActionJson action : actions) {
            String actionResponseText = processLevel2Action(relevanteAccount, action);
            actionsResponses.add(actionResponseText);
        }
        return actionsResponses;
    }

    public String processLevel2Action(RelevanteAccount relevanteAccount,
                                      Level2ActionJson action) {

        Network network = new FindByNetworkName(networks).find(action.getNetwork());
        NetworkActionService actionService = new FindByNetwork<>(networkActionServices).find(network);
        String actionResponseText = actionService.postLevel2Action(relevanteAccount.getId(), action.getPostId(), action.getCommentText());
        return actionResponseText;
    }

    public List<String> processLevel3Actions(String relevanteId,
                                             List<Level3ActionJson> actions) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        List<String> actionsResponses = new ArrayList<>();
        for (Level3ActionJson action : actions) {
            String actionResponseText = processLevel3Action(relevanteAccount, action);
            actionsResponses.add(actionResponseText);
        }
        return actionsResponses;
    }

    public String processLevel3Action(RelevanteAccount relevanteAccount,
                                      Level3ActionJson action) {

        Network network = new FindByNetworkName(networks).find(action.getNetwork());
        NetworkActionService actionService = new FindByNetwork<>(networkActionServices).find(network);
        String actionResponseText = actionService.putLevel3Action(relevanteAccount.getId(), action.getUserId());
        return actionResponseText;
    }

    public List<String> processLevel4Actions(String relevanteId,
                                             List<Level4ActionJson> actions) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        List<String> actionsResponses = new ArrayList<>();
        for (Level4ActionJson action : actions) {
            String actionResponseText = processLevel4Action(relevanteAccount, action);
            actionsResponses.add(actionResponseText);
        }
        return actionsResponses;
    }

    public String processLevel4Action(RelevanteAccount relevanteAccount,
                                      Level4ActionJson action) {

        Network network = new FindByNetworkName(networks).find(action.getNetwork());
        NetworkActionService actionService = new FindByNetwork<>(networkActionServices).find(network);
        String actionResponseText = actionService.postLevel4Action(relevanteAccount.getId(), action.getUserId(), action.getSubject(), action.getMessage());
        return actionResponseText;
    }

}
