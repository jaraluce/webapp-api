package me.relevante.web.service;

import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteUserService {

    private EmailPermissionsService emailPermissionsService;
    private RelevanteAccountRepo relevanteAccountRepo;
    private RelevanteContextRepo relevanteContextRepo;

    @Autowired
    public DeleteUserService(EmailPermissionsService emailPermissionsService,
                             RelevanteAccountRepo relevanteAccountRepo,
                             RelevanteContextRepo relevanteContextRepo) {
        this.emailPermissionsService = emailPermissionsService;
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
    }

    public boolean deleteRelevanteAccount(String relevanteId) {
        if (emailPermissionsService.isForbiddenToDelete(relevanteId)) {
            return false;
        }
        relevanteAccountRepo.delete(relevanteId);
        relevanteContextRepo.delete(relevanteId);
        return true;
    }
}
