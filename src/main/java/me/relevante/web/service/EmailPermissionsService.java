package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.web.model.EmailPermissions;
import me.relevante.web.persistence.EmailPermissionsRepo;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailPermissionsService {

    private EmailPermissionsRepo emailPermissionsRepo;
    private RelevanteAccountRepo relevanteAccountRepo;

    @Autowired
    public EmailPermissionsService(EmailPermissionsRepo emailPermissionsRepo,
                                   RelevanteAccountRepo relevanteAccountRepo) {
        this.emailPermissionsRepo = emailPermissionsRepo;
        this.relevanteAccountRepo = relevanteAccountRepo;
    }

    public boolean isAdmin(String relevanteId) {
        EmailPermissions emailPermissions = findEmailPermissions(relevanteId);
        return (emailPermissions != null && emailPermissions.isAdmin());
    }

    public boolean isAllowedToLogin(String relevanteId) {
        EmailPermissions emailPermissions = findEmailPermissions(relevanteId);
        return (emailPermissions != null && emailPermissions.isAllowedToLogin());
    }

    public boolean isForbiddenToLogin(String relevanteId) {
        return !isAllowedToLogin(relevanteId);
    }

    public boolean isAllowedToDelete(String relevanteId) {
        EmailPermissions emailPermissions = findEmailPermissions(relevanteId);
        return (emailPermissions != null && emailPermissions.isAllowedToDelete());
    }

    public EmailPermissions findEmailPermissions(String relevanteId) {
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        if (relevanteAccount == null) {
            return null;
        }
        if (relevanteAccount.getEmail() == null) {
            return null;
        }
        EmailPermissions emailPermissions = emailPermissionsRepo.findOneByEmail(relevanteAccount.getEmail().toLowerCase());
        return emailPermissions;
    }

    public boolean isForbiddenToDelete(String relevanteId) {
        return !isAllowedToDelete(relevanteId);
    }

    public void allowLogin(String email) {
        setAllowedToLogin(email.toLowerCase(), true);
    }

    public void denyLogin(String email) {
        setAllowedToLogin(email.toLowerCase(), false);
    }

    private void setAllowedToLogin(String email,
                                   boolean allowedToLogin) {
        EmailPermissions emailPermissions = emailPermissionsRepo.findOneByEmail(email.toLowerCase());
        if (emailPermissions == null) {
            emailPermissions = new EmailPermissions(email.toLowerCase(), false, false, false);
        }
        emailPermissions.setAllowedToLogin(allowedToLogin);
        emailPermissionsRepo.save(emailPermissions);
    }

}
