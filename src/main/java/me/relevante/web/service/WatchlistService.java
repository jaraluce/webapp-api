package me.relevante.web.service;

import me.relevante.model.Blacklist;
import me.relevante.model.RelevanteContext;
import me.relevante.model.Watchlist;
import me.relevante.model.WatchlistUser;
import me.relevante.web.model.json.WatchlistJson;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WatchlistService {

    @Autowired private RelevanteContextRepo relevanteContextRepo;

    public List<WatchlistJson> getAllWatchlists(String relevanteId) {
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        List<Watchlist> watchlists = relevanteContext.getWatchlists();
        Map<String, Long> watchlistsCount = getWatchlistsCount(relevanteId);
        List<WatchlistJson> watchlistJsons = new ArrayList<>();
        for (Watchlist watchlist : watchlists) {
            WatchlistJson watchlistJson = createWatchlistJsonFromWatchlist(watchlist);
            Long count = watchlistsCount.get(watchlist.getId());
            watchlistJson.setNum(count);
            watchlistJsons.add(watchlistJson);
        }
        return watchlistJsons;
    }

    public WatchlistJson getWatchlistById(String relevanteId,
                                          String watchlistId) {
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistById(watchlistId);
        WatchlistJson watchlistJson = createWatchlistJsonFromWatchlist(watchlist);
        return watchlistJson;
    }

    public WatchlistJson getWatchlistByRelevanteIdAndName(String relevanteId,
                                                          String watchlistName) {
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistByName(watchlistName);
        WatchlistJson watchlistJson = createWatchlistJsonFromWatchlist(watchlist);
        return watchlistJson;
    }

    public WatchlistJson createWatchlist(String relevanteId,
                                         WatchlistJson watchlistToCreate) {
        validateSearchTerms(watchlistToCreate.getSearchTerms());
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = new Watchlist(relevanteContext.getRelevanteId(), watchlistToCreate.getName(), watchlistToCreate.getSearchTerms());
        if (relevanteContext.addWatchlist(watchlist) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
        WatchlistJson watchlistJson = createWatchlistJsonFromWatchlist(watchlist);
        return watchlistJson;
    }

    public void updateWatchlist(String relevanteId,
                                String watchlistId,
                                WatchlistJson watchlistToUpdate) {
        validateSearchTerms(watchlistToUpdate.getSearchTerms());
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistById(watchlistId);
        if (watchlist == null) {
            throw new IllegalArgumentException();
        }
        Watchlist watchlistByName = relevanteContext.getWatchlistByName(watchlistToUpdate.getName());
        if (watchlistByName != null && !watchlistByName.getId().equals(watchlistId)) {
            throw new IllegalArgumentException();
        }
        watchlist.setName(watchlistToUpdate.getName());
        watchlist.getSearchTerms().clear();
        watchlist.getSearchTerms().addAll(watchlistToUpdate.getSearchTerms());
        relevanteContextRepo.save(relevanteContext);
    }

    public void deleteWatchlist(String relevanteId,
                                String watchlistId) {
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistById(watchlistId);
        if (watchlist != null) {
            relevanteContext.removeWatchlistById(watchlistId);
            relevanteContextRepo.save(relevanteContext);
        }
    }

    public Map<String, Long> getWatchlistsCount(String relevanteId) {

        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Blacklist blacklist = relevanteContext.getBlacklist();

        Map<String, Long> watchlistsCount = new HashMap<>();
        for (Watchlist watchlist : relevanteContext.getWatchlists()) {
            List<WatchlistUser> watchlistUsers = relevanteContext.getWatchlistUsersInWatchlist(watchlist.getId());
            long watchlistUsersCount = 0;
            for (WatchlistUser watchlistUser : watchlistUsers) {
                if (blacklist.isBlacklistUser(watchlistUser.getNetworkUser()))
                    continue;
                watchlistUsersCount++;
            }
            watchlistsCount.put(watchlist.getId(), watchlistUsersCount);
        }
        return watchlistsCount;
    }

    private void validateSearchTerms(List<String> searchTerms) {
        if (searchTerms == null) {
            throw new IllegalArgumentException("Search Terms collection can not be null");
        }
        if (searchTerms.isEmpty()) {
            throw new IllegalArgumentException("Search Terms collection can not be empty");
        }
        for (String searchTerm : searchTerms) {
            if (StringUtils.isBlank(searchTerm)) {
                throw new IllegalArgumentException("Search Terms can not be blank");
            }
        }
    }

    private WatchlistJson createWatchlistJsonFromWatchlist(Watchlist watchlist) {
        WatchlistJson watchlistJson = new WatchlistJson();
        watchlistJson.setName(watchlist.getName());
        watchlistJson.getSearchTerms().addAll(watchlist.getSearchTerms());
        watchlistJson.setWatchlistId(watchlist.getId());
        return watchlistJson;
    }

}
