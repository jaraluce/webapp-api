package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.network.Network;
import me.relevante.web.model.json.NetworkActionJson;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PendingActionsService {

    private RelevanteAccountRepo relevanteAccountRepo;
    private List<NetworkPendingActionsService> pendingActionsServices;

    @Autowired
    public PendingActionsService(RelevanteAccountRepo relevanteAccountRepo,
                                 List<NetworkPendingActionsService> pendingActionsServices) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.pendingActionsServices = new ArrayList<>(pendingActionsServices);
    }

    public List<NetworkActionJson> getPendingActions(String relevanteId) {
        List<NetworkActionJson> pendingActions = new ArrayList<>();
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        for (NetworkPendingActionsService pendingActionsService : pendingActionsServices) {
            Network network = pendingActionsService.getNetwork();
            if (!relevanteAccount.isNetworkConnected(network))
                continue;
            String networkUserId = relevanteAccount.getCredentials(network).getUserId();
            List<NetworkActionJson> networkPendingActions = pendingActionsService.getPendingActions(networkUserId);
            pendingActions.addAll(networkPendingActions);
        }
        return pendingActions;
    }

    public void acknowledgeActionDone(String network,
                                      String actionName,
                                      String actionId) {

        for (NetworkPendingActionsService pendingActionsService : pendingActionsServices) {
            if (!pendingActionsService.getNetwork().getName().equals(network))
                continue;
            pendingActionsService.acknowledgeActionDone(actionName, actionId);
        }
    }

    public void acknowledgeActionError(String network,
                                      String actionName,
                                      String actionId) {

        for (NetworkPendingActionsService pendingActionsService : pendingActionsServices) {
            if (!pendingActionsService.getNetwork().getName().equals(network))
                continue;
            pendingActionsService.acknowledgeActionError(actionName, actionId);
        }
    }

}
