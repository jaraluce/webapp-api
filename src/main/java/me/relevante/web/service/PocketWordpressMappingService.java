package me.relevante.web.service;

import me.relevante.model.PocketWordpressMapping;
import me.relevante.web.model.json.PocketWordpressMappingJson;
import me.relevante.web.persistence.PocketWordpressMappingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PocketWordpressMappingService {

    @Autowired
    private PocketWordpressMappingRepo mappingRepo;

    public List<PocketWordpressMappingJson> getAllMappingsByHub(String relevanteId,
                                                                String hubId) {
        List<PocketWordpressMapping> mappings = mappingRepo.findByRelevanteIdAndWpHubId(relevanteId, hubId);
        List<PocketWordpressMappingJson> mappingsJson = new ArrayList<>();
        mappings.forEach(mapping -> mappingsJson.add(mapMappingToJsonMapping(mapping)));
        return mappingsJson;
    }

    public void putMapping(String relevanteId,
                           String wpHubId,
                           PocketWordpressMappingJson mappingJson) {
        List<PocketWordpressMapping> mappings = mappingRepo.findByRelevanteIdAndWpHubId(relevanteId, wpHubId);
        for (PocketWordpressMapping mapping : mappings) {
            if (mapping.getWpCategory().equals(mappingJson.getMatch()) && mapping.getPocketTag().equals(mappingJson.getPocket())) {
                mappingJson.setId(mapping.getId());
                return;
            }
        }
        PocketWordpressMapping mapping = mapJsonMappingToMapping(relevanteId, wpHubId, mappingJson);
        mapping = mappingRepo.save(mapping);
        mappingJson.setId(mapping.getId());
    }

    public void deleteMapping(String relevanteId,
                              String wpHubId,
                              String mappingId) {
        PocketWordpressMapping mapping = mappingRepo.findOne(mappingId);
        if (!mapping.getRelevanteId().equals(relevanteId)) {
            throw new IllegalArgumentException();
        }
        if (!mapping.getWpHubId().equals(wpHubId)) {
            throw new IllegalArgumentException();
        }
        mappingRepo.delete(mappingId);
    }

    private PocketWordpressMappingJson mapMappingToJsonMapping(PocketWordpressMapping mapping) {
        PocketWordpressMappingJson mappingJson = new PocketWordpressMappingJson();
        mappingJson.setId(mapping.getId());
        mappingJson.setPocket(mapping.getPocketTag());
        mappingJson.setMatch(mapping.getWpCategory());
        return mappingJson;
    }

    private PocketWordpressMapping mapJsonMappingToMapping(String relevanteId,
                                                           String wpHubId,
                                                           PocketWordpressMappingJson mappingJson) {
        PocketWordpressMapping mapping = new PocketWordpressMapping(relevanteId,
                mappingJson.getPocket(),
                mappingJson.getMatch(),
                wpHubId);
        return mapping;
    }

}
