package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.web.model.json.AccountUpdateRequest;
import me.relevante.web.model.json.NetworkAccountData;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountDataService {

    private RelevanteAccountRepo relevanteAccountRepo;
    private List<NetworkAccountDataService> networkAccountDataServices;

    @Autowired
    public AccountDataService(RelevanteAccountRepo relevanteAccountRepo,
                              List<NetworkAccountDataService> networkAccountDataServices) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.networkAccountDataServices = new ArrayList<>(networkAccountDataServices);
    }

    public JSONObject getData(String relevanteId) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        JSONObject accountData = mapRelevanteAccountToAccountData(relevanteAccount);
        for (NetworkAccountDataService networkAccountDataService : networkAccountDataServices) {
            if (!relevanteAccount.isNetworkConnected(networkAccountDataService.getNetwork())) {
                continue;
            }
            NetworkAccountData networkAccountData = networkAccountDataService.getData(relevanteAccount);
            accountData.put(networkAccountDataService.getNetwork().getName(), networkAccountData);
        }
        return accountData;
    }

    public HttpStatus updateData(String relevanteId,
                                 AccountUpdateRequest updateRequest) {

        if (relevanteId == null || updateRequest == null) {
            throw new IllegalArgumentException();
        }
        if (StringUtils.isBlank(updateRequest.getName())) {
            return HttpStatus.NOT_ACCEPTABLE;
        }
        if (StringUtils.isBlank(updateRequest.getEmail())) {
            return HttpStatus.NOT_ACCEPTABLE;
        }
        if (StringUtils.isBlank(updateRequest.getPassword())) {
            return HttpStatus.NOT_ACCEPTABLE;
        }

        RelevanteAccount relevanteAccountWithUpdatedEmail = relevanteAccountRepo.findOneByEmail(updateRequest.getEmail());
        if (relevanteAccountWithUpdatedEmail != null && !relevanteAccountWithUpdatedEmail.getId().equals(relevanteId)) {
            return HttpStatus.CONFLICT;
        }

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        relevanteAccount.setName(updateRequest.getName());
        relevanteAccount.setEmail(updateRequest.getEmail());
        relevanteAccount.setPassword(updateRequest.getPassword());
        relevanteAccountRepo.save(relevanteAccount);

        return HttpStatus.OK;
    }

    private JSONObject mapRelevanteAccountToAccountData(RelevanteAccount relevanteAccount) {
        JSONObject accountData = new JSONObject();
        accountData.put("id", relevanteAccount.getId());
        accountData.put("name", relevanteAccount.getName());
        accountData.put("email", relevanteAccount.getEmail());
        accountData.put("picture", relevanteAccount.getImageUrl());
        accountData.put("isNewUser", false);
        accountData.put("hubEnabled", relevanteAccount.isHubEnabled());
        accountData.put("bulkEngageEnabled", relevanteAccount.isBulkEngageEnabled());
        return accountData;
    }

}