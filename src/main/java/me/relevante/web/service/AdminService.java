package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.web.model.json.EnableUserRequest;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {

    private WordpressConnectService wordpressConnectService;
    private RelevanteAccountRepo relevanteAccountRepo;
    private EmailPermissionsService emailPermissionsService;

    @Autowired
    public AdminService(WordpressConnectService wordpressConnectService,
                        RelevanteAccountRepo relevanteAccountRepo,
                        EmailPermissionsService emailPermissionsService) {
        this.wordpressConnectService = wordpressConnectService;
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.emailPermissionsService = emailPermissionsService;
    }

    public List<String> enableUsers(String adminRelevanteId,
                                    EnableUserRequest enableUserRequest) throws UnsupportedOperationException {
        if (!emailPermissionsService.isAdmin(adminRelevanteId)) {
            throw new UnsupportedOperationException();
        }
        List<String> notFoundEmails = new ArrayList<>();
        for (String email : enableUserRequest.getEmails()) {
            emailPermissionsService.allowLogin(email);
            RelevanteAccount relevanteAccount = relevanteAccountRepo.findOneByEmail(email);
            if (relevanteAccount == null) {
                notFoundEmails.add(email);
                continue;
            }
            if (enableUserRequest.getHubUrl() != null) {
                relevanteAccount.setHubEnabled(true);
                wordpressConnectService.connectNetworkByBasicAuth(relevanteAccount.getId(), enableUserRequest.getHubUrl(), enableUserRequest.getUsername(), enableUserRequest.getPassword());
            }
            relevanteAccount.setBulkEngageEnabled(enableUserRequest.isBulkEngageEnabled());
            relevanteAccountRepo.save(relevanteAccount);
        }
        return notFoundEmails;
    }

}
