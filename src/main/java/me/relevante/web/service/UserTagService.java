package me.relevante.web.service;

import me.relevante.model.NetworkUser;
import me.relevante.model.RelevanteContext;
import me.relevante.model.UserTag;
import me.relevante.web.model.UserIdDecoder;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserTagService {

    private RelevanteContextRepo relevanteContextRepo;
    private UserIdDecoder userIdDecoder;

    @Autowired
    public UserTagService(RelevanteContextRepo relevanteContextRepo,
                          UserIdDecoder userIdDecoder) {
        this.relevanteContextRepo = relevanteContextRepo;
        this.userIdDecoder = userIdDecoder;
    }

    public void saveUserTag(String relevanteId,
                            String encodedUserId,
                            String tag) {

        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        NetworkUser networkUser = userIdDecoder.decode(encodedUserId);
        UserTag userTag = new UserTag(relevanteContext.getRelevanteId(), networkUser.getNetwork(), networkUser.getUserId(), tag);
        if (relevanteContext.addUserTag(userTag) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
    }

    public void saveUsersTag(String relevanteId,
                             List<String> encodedUserIds,
                             String tag) {

        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        for (String encodedUserId : encodedUserIds) {
            NetworkUser networkUser = userIdDecoder.decode(encodedUserId);
            UserTag userTag = new UserTag(relevanteContext.getRelevanteId(), networkUser.getNetwork(), networkUser.getUserId(), tag);
            relevanteContext.addUserTag(userTag);
        }
        relevanteContextRepo.save(relevanteContext);
    }

    public void deleteUserTag(String relevanteId,
                              String encodedUserId,
                              String tag) {

        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        NetworkUser networkUser = userIdDecoder.decode(encodedUserId);
        UserTag userTag = new UserTag(relevanteContext.getRelevanteId(), networkUser.getNetwork(), networkUser.getUserId(), tag);
        if (relevanteContext.removeUserTag(userTag) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
    }

    public Map<String, Integer> getTagAppearances(String relevanteId) {

        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Map<String, Integer> appearancesByNote = new HashMap();

        // Count the number of times this note appears
        for (UserTag userTag : relevanteContext.getUserTags()) {
            Integer appearances = appearancesByNote.get(userTag.getTag());
            if (appearances == null)
                appearances = 0;
            appearances++;
            appearancesByNote.put(userTag.getTag(), appearances);
        }

        return appearancesByNote;
    }
}
