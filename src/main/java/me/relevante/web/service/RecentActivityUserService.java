package me.relevante.web.service;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.utils.ListsMerger;
import me.relevante.web.model.CardsConverter;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecentActivityUserService {

    private RelevanteAccountRepo relevanteAccountRepo;
    private RelevanteContextRepo relevanteContextRepo;
    private List<NetworkRecentActivityService> networkRecentActivityServices;
    private ListsMerger<NetworkFullUser> listsMerger;
    private CardsConverter cardsConverter;

    @Autowired
    public RecentActivityUserService(RelevanteAccountRepo relevanteAccountRepo,
                                     RelevanteContextRepo relevanteContextRepo,
                                     List<NetworkRecentActivityService> networkRecentActivityServices,
                                     ListsMerger<NetworkFullUser> listsMerger,
                                     CardsConverter cardsConverter) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.networkRecentActivityServices = new ArrayList<>(networkRecentActivityServices);
        this.listsMerger = listsMerger;
        this.cardsConverter = cardsConverter;
    }

    public List<NetworkCardUser> getRecentlyUpdatedUsers(String relevanteId,
                                                         Integer maxElements) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        List<NetworkFullUser> recentlyUpdatedUsers = createRecentlyUpdatedUsersList(relevanteAccount, relevanteContext);

        // Do pagination
        maxElements = (maxElements == null) ? Integer.MAX_VALUE : maxElements;
        int endIndex = Math.min(maxElements, recentlyUpdatedUsers.size());
        List<NetworkFullUser> paginatedUsers = new ArrayList<>(recentlyUpdatedUsers.subList(0, endIndex));

        List<NetworkCardUser> cardUsers = cardsConverter.convertFullUsers(paginatedUsers, relevanteAccount, relevanteContext);

        return cardUsers;
    }

    private List<NetworkFullUser> createRecentlyUpdatedUsersList(RelevanteAccount relevanteAccount,
                                                                 RelevanteContext relevanteContext) {

        List<List<? extends NetworkFullUser>> networkFullUsers = new ArrayList<>();
        for (NetworkRecentActivityService networkRecentActivityService : networkRecentActivityServices) {
            List<NetworkFullUser> fullUsers = networkRecentActivityService.getNetworkRecentlyUpdatedUsers(relevanteAccount, relevanteContext);
            networkFullUsers.add(fullUsers);
        }

        List<NetworkFullUser> allUsers = listsMerger.mergeAlternatingElements(networkFullUsers);

        return allUsers;
    }

}
