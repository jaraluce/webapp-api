package me.relevante.web.service;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.Watchlist;
import me.relevante.model.WatchlistUser;
import me.relevante.utils.ListsMerger;
import me.relevante.web.model.CardsConverter;
import me.relevante.web.model.FullUserWithPostsComparator;
import me.relevante.web.model.UserIdDecoder;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WatchlistUserService {

    private RelevanteAccountRepo relevanteAccountRepo;
    private RelevanteContextRepo relevanteContextRepo;
    private List<NetworkWatchlistUserService> networkWatchlistUserServices;
    private ListsMerger<NetworkFullUser> listsMerger;
    private FullUserWithPostsComparator networkFullUserComparator;
    private CardsConverter cardsConverter;
    private UserIdDecoder userIdDecoder;

    @Autowired
    public WatchlistUserService(RelevanteAccountRepo relevanteAccountRepo,
                                RelevanteContextRepo relevanteContextRepo,
                                List<NetworkWatchlistUserService> networkWatchlistUserServices,
                                ListsMerger<NetworkFullUser> listsMerger,
                                FullUserWithPostsComparator networkFullUserComparator,
                                CardsConverter cardsConverter,
                                UserIdDecoder userIdDecoder) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.networkWatchlistUserServices = new ArrayList<>(networkWatchlistUserServices);
        this.listsMerger = listsMerger;
        this.networkFullUserComparator = networkFullUserComparator;
        this.cardsConverter = cardsConverter;
        this.userIdDecoder = userIdDecoder;
    }

    public List<NetworkCardUser> getWatchlistUsers(String relevanteId,
                                                   String watchlistId,
                                                   Integer maxElements) {
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);

        List<List<? extends NetworkFullUser>> networkFullUsers = new ArrayList<>();
        for (NetworkWatchlistUserService networkWatchlistUserService : networkWatchlistUserServices) {
            List<NetworkFullUser> fullUsers = networkWatchlistUserService.getNetworkWatchlistUsers(relevanteAccount, relevanteContext, watchlistId);
            networkFullUsers.add(fullUsers);
        }
        List<NetworkFullUser> allUsers = listsMerger.mergeAlternatingElements(networkFullUsers);

        // Sort by most recent activity
        allUsers.sort(networkFullUserComparator);

        // Do pagination
        maxElements = (maxElements == null) ? Integer.MAX_VALUE : maxElements;
        int endIndex = Math.min(maxElements, allUsers.size());
        List<NetworkFullUser> paginatedUsers = new ArrayList<>(allUsers.subList(0, endIndex));

        List<NetworkCardUser> cardUsers = cardsConverter.convertFullUsers(paginatedUsers, relevanteAccount, relevanteContext);

        return cardUsers;
    }

    public void addUserToWatchlist(String relevanteId,
                                   String watchlistId,
                                   String encodedUserId) {

        NetworkUser networkUser = userIdDecoder.decode(encodedUserId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistById(watchlistId);
        WatchlistUser watchlistUser = new WatchlistUser(watchlist.getId(), networkUser.getNetwork(), networkUser.getUserId(), null);
        if (relevanteContext.addWatchlistUser(watchlistUser) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
    }

    public void addUsersToWatchlist(String relevanteId,
                                    String watchlistId,
                                    List<String> encodedUserIds) {

        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistById(watchlistId);
        Date aYearAgo = Date.from(LocalDateTime.now().minusYears(1).toInstant(ZoneOffset.UTC));
        for (String encodedUserId : encodedUserIds) {
            NetworkUser networkUser = userIdDecoder.decode(encodedUserId);
            WatchlistUser watchlistUser = new WatchlistUser(watchlist.getId(), networkUser.getNetwork(), networkUser.getUserId(), null);
            relevanteContext.addWatchlistUser(watchlistUser);
            relevanteContext.setLastMonitoringTimestamp(aYearAgo);
        }
        relevanteContextRepo.save(relevanteContext);
    }

    public void removeUserFromWatchlist(String relevanteId,
                                        String watchlistId,
                                        String encodedUserId) {

        NetworkUser networkUser = userIdDecoder.decode(encodedUserId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        Watchlist watchlist = relevanteContext.getWatchlistById(watchlistId);
        WatchlistUser watchlistUser = new WatchlistUser(watchlist.getId(), networkUser.getNetwork(), networkUser.getUserId(), null);
        if (relevanteContext.removeWatchlistUser(watchlistUser) != null) {
            relevanteContextRepo.save(relevanteContext);
        }
    }

}
