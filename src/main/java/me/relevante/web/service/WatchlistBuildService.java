package me.relevante.web.service;

import me.relevante.model.Blacklist;
import me.relevante.model.NetworkSearchUser;
import me.relevante.model.NetworkUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.WatchlistUser;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Network;
import me.relevante.utils.Chronometer;
import me.relevante.utils.ListsMerger;
import me.relevante.web.model.CardsConverter;
import me.relevante.web.model.filter.OutOfBlacklistFilter;
import me.relevante.web.model.filter.OutOfListFilter;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.model.search.NetworkSearcher;
import me.relevante.web.model.utils.FindByNetwork;
import me.relevante.web.model.validation.NetworkSearchUserValidator;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WatchlistBuildService {

    private static final Logger logger = LoggerFactory.getLogger(WatchlistBuildService.class);

    private RelevanteAccountRepo relevanteAccountRepo;
    private RelevanteContextRepo relevanteContextRepo;
    private ListsMerger<NetworkSearchUser> listsMerger;
    private List<NetworkSearcher> networkSearchers;
    private List<NetworkSearchUserValidator> networkSearchUserValidators;
    private CardsConverter cardsConverter;

    @Autowired
    public WatchlistBuildService(RelevanteAccountRepo relevanteAccountRepo,
                                 RelevanteContextRepo relevanteContextRepo,
                                 ListsMerger<NetworkSearchUser> listsMerger,
                                 List<NetworkSearcher> networkSearchers,
                                 List<NetworkSearchUserValidator> networkSearchUserValidators,
                                 CardsConverter cardsConverter) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.listsMerger = listsMerger;
        this.networkSearchers = new ArrayList<>(networkSearchers);
        this.networkSearchUserValidators = new ArrayList<>(networkSearchUserValidators);
        this.cardsConverter = cardsConverter;
    }

    public List<NetworkCardUser> getBuildUsers(String relevanteId,
                                               String watchlistId,
                                               Integer maxElements) {

        Chronometer chrono = new Chronometer();

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        List<NetworkSearchUser> allFilteredUsers = obtainBuildUsers(relevanteContext, watchlistId);

        maxElements = (maxElements == null) ? Integer.MAX_VALUE : maxElements;
        int endIndex = Math.min(maxElements, allFilteredUsers.size());
        List<NetworkSearchUser> paginatedUsers = new ArrayList<>(allFilteredUsers.subList(0, endIndex));
        List<NetworkCardUser> cardUsers = cardsConverter.convertSearchUsers(paginatedUsers, relevanteAccount, relevanteContext);

        logger.info("Elapsed time building Watchlist " + watchlistId + " users is: " + chrono.getElapsedTimeText());

        return cardUsers;
    }

    private List<NetworkSearchUser> obtainBuildUsers(RelevanteContext relevanteContext,
                                                     String watchlistId) {

        List<String> searchTerms = relevanteContext.getWatchlistById(watchlistId).getSearchTerms();

        // Select users out of blacklist, and with information enough to appear in the results
        Blacklist blacklist = relevanteContext.getBlacklist();
        List<NetworkUser> alreadyExistingUsers = new ArrayList<>();
        List<WatchlistUser> watchlistUsers = relevanteContext.getWatchlistUsersInWatchlist(watchlistId);
        for (WatchlistUser watchlistUser : watchlistUsers) {
            NetworkUser networkUser = new NetworkUser() {
                @Override
                public String getUserId() {
                    return watchlistUser.getUserId();
                }

                @Override
                public Network getNetwork() {
                    return watchlistUser.getNetwork();
                }
            };
            alreadyExistingUsers.add(networkUser);
        }

        List<List<? extends NetworkSearchUser>> networkSelectedUsers = new ArrayList<>();
        for (NetworkSearcher networkSearcher : networkSearchers) {
            Network network = networkSearcher.getNetwork();
            List<? extends NetworkSearchUser> searchUsers = networkSearcher.execute(searchTerms);
            NetworkSearchUserValidator networkUserValidator = new FindByNetwork<>(networkSearchUserValidators).find(network);
            List<? extends NetworkSearchUser> selectedUsers  = new FilterChain<NetworkUser>()
                    .add(new ValidationFilter<>(networkUserValidator))
                    .add(new OutOfBlacklistFilter(blacklist))
                    .add(new OutOfListFilter<>(alreadyExistingUsers))
                    .execute(searchUsers);
            networkSelectedUsers.add(selectedUsers);
        }

        List<NetworkSearchUser> allUsers = listsMerger.mergeAlternatingElements(networkSelectedUsers);

        return allUsers;
    }

}
