package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.EnableUserRequest;
import me.relevante.web.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author daniel-ibanez
 */
@RestController
@RequestMapping(value = Route.API_ADMIN)
public class AdminController {

    @Autowired private AdminService adminService;

    @RequestMapping(value = "/enableUser",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> enableUser(@RequestBody EnableUserRequest enableUserRequest,
                                                   HttpSession session) {

        String adminRelevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<String> notFoundEmails = adminService.enableUsers(adminRelevanteId, enableUserRequest);
        if (notFoundEmails.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity(notFoundEmails, HttpStatus.NOT_FOUND);
    }

}
