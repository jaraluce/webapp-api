package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.service.WatchlistUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.io.InvalidObjectException;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_WATCHLIST_USERS_BY_WATCHLIST)
public class WatchlistUserController {

    @Autowired private WatchlistUserService watchlistUserService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NetworkCardUser> getAll(@PathVariable String watchlistId,
                                        @RequestParam(name = "total", required = false) Integer maxElements,
                                        HttpSession session) throws InvalidObjectException {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<NetworkCardUser> watchlistUsers = watchlistUserService.getWatchlistUsers(relevanteId, watchlistId, maxElements);

        return watchlistUsers;
    }

    @RequestMapping(value = "/{encodedUserId}",
                    method = RequestMethod.PUT)
    public void addUserToWatchlist(@PathVariable String watchlistId,
                                   @PathVariable String encodedUserId,
                                   HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        watchlistUserService.addUserToWatchlist(relevanteId, watchlistId, encodedUserId);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void addUsersToWatchlist(@PathVariable String watchlistId,
                                    @RequestBody List<String> encodedUserIds,
                                    HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        watchlistUserService.addUsersToWatchlist(relevanteId, watchlistId, encodedUserIds);
    }

    @RequestMapping(value = "/{encodedUserId}",
                    method = RequestMethod.DELETE)
    public void removeUserFromWatchlist(@PathVariable String watchlistId,
                                        @PathVariable String encodedUserId,
                                        HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        watchlistUserService.removeUserFromWatchlist(relevanteId, watchlistId, encodedUserId);
    }


}
