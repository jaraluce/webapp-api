package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.service.BlacklistUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.io.InvalidObjectException;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_BLACKLIST)
public class BlacklistUserController {

    @Autowired private BlacklistUserService blacklistUserService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NetworkCardUser> getAllUsers(@RequestParam(name = "total", required = false) Integer maxElements,
                                             HttpSession session) throws InvalidObjectException {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<NetworkCardUser> blacklistUsers = blacklistUserService.getBlacklistUsers(relevanteId, maxElements);
        return blacklistUsers;
    }

    @RequestMapping(value = "/{encodedUserId}",
                    method = RequestMethod.PUT)
    public void addUser(@PathVariable String encodedUserId,
                        HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        blacklistUserService.addUserToBlacklist(relevanteId, encodedUserId);
    }

    @RequestMapping(value = "/{encodedUserId}",
                    method = RequestMethod.DELETE)
    public void removeUser(@PathVariable String encodedUserId,
                           HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        blacklistUserService.removeUserFromBlacklist(relevanteId, encodedUserId);
    }
}
