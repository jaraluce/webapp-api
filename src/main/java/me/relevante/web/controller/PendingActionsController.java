package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.NetworkActionJson;
import me.relevante.web.service.PendingActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_PENDING_ACTIONS)
public class PendingActionsController {

    @Autowired private PendingActionsService pendingActionsService;

    @RequestMapping(method = RequestMethod.GET)
    public List<NetworkActionJson> getPendingActions(HttpSession session) {
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<NetworkActionJson> pendingActions = pendingActionsService.getPendingActions(relevanteId);
        return pendingActions;
    }

    @RequestMapping(value = "/{network}/{actionName}/{actionId}/done",
                    method = RequestMethod.PUT)
    public void acknowledgeActionDone(@PathVariable String network,
                                      @PathVariable String actionName,
                                      @PathVariable String actionId) {
        pendingActionsService.acknowledgeActionDone(network, actionName, actionId);
    }

    @RequestMapping(value = "/{network}/{actionName}/{actionId}/error",
            method = RequestMethod.PUT)
    public void acknowledgeActionError(@PathVariable String network,
                                       @PathVariable String actionName,
                                       @PathVariable String actionId) {
        pendingActionsService.acknowledgeActionError(network, actionName, actionId);
    }

}