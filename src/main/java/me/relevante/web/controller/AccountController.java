package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.AccountUpdateRequest;
import me.relevante.web.model.json.Level1ActionJson;
import me.relevante.web.service.AccountDataService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_ACCOUNT)
public class AccountController {

    @Autowired private AccountDataService accountDataService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject getData(HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        JSONObject accountData = accountDataService.getData(relevanteId);
        return accountData;
    }

    @RequestMapping(method = RequestMethod.PUT,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getData(@RequestBody AccountUpdateRequest updateRequest,
                                  HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        HttpStatus httpStatus = accountDataService.updateData(relevanteId, updateRequest);
        ResponseEntity response = new ResponseEntity(httpStatus);
        return response;
    }

}