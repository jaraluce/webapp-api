package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.service.RecentActivityUserService;
import me.relevante.web.service.UserTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_USERS)
public class UserController {

    @Autowired private RecentActivityUserService recentActivityUserService;
    @Autowired private UserTagService userTagService;

    @RequestMapping(value = "/recentlyUpdated",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NetworkCardUser> getRecentlyUpdatedUsers(@RequestParam(name = "total", required = false) Integer maxElements,
                                                         HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<NetworkCardUser> recentlyUpdatedUsers = recentActivityUserService.getRecentlyUpdatedUsers(relevanteId, maxElements);

        return recentlyUpdatedUsers;
    }

    @RequestMapping(value = "/{encodedUserId}/tags",
                    method = RequestMethod.POST)
    public void createUserTag(@PathVariable String encodedUserId,
                              @RequestBody String tag,
                              HttpSession session) throws Exception {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        userTagService.saveUserTag(relevanteId, encodedUserId, tag);
    }

    @RequestMapping(value = "/{encodedUserId}/tags/{tag}",
                    method = RequestMethod.DELETE)
    public void deleteUserTag(@PathVariable String encodedUserId,
                              @PathVariable String tag,
                              HttpSession session) throws Exception {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        userTagService.deleteUserTag(relevanteId, encodedUserId, tag);
    }

}