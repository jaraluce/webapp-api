package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.card.NetworkCardUser;
import me.relevante.web.service.WatchlistBuildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_WATCHLISTS)
public class WatchlistBuildController {

    @Autowired private WatchlistBuildService watchlistBuildService;

    @RequestMapping(value = "/{watchlistId}/build",
                    method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NetworkCardUser> getBuildUsers(@PathVariable String watchlistId,
                                               @RequestParam(name = "total", required = false) Integer maxElements,
                                               HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<NetworkCardUser> buildUsers = watchlistBuildService.getBuildUsers(relevanteId, watchlistId, maxElements);
        return buildUsers;
    }

}