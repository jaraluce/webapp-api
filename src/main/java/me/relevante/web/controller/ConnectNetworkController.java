package me.relevante.web.controller;

import me.relevante.network.AbstractNetwork;
import me.relevante.network.Network;
import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.BasicAuthConnectRequest;
import me.relevante.web.model.json.OAuthConnectRequest;
import me.relevante.web.model.oauth.NetworkOAuthManager;
import me.relevante.web.model.oauth.OAuthAccessRequest;
import me.relevante.web.model.utils.FindByNetwork;
import me.relevante.web.service.NetworkConnectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class ConnectNetworkController {

    @Autowired private List<NetworkOAuthManager> oAuthManagers;
    @Autowired private List<NetworkConnectService> connectServices;

    @RequestMapping(value = Route.API_CONNECT_NETWORK + "/oAuthRequest",
                    method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public OAuthAccessRequest obtainOAuthRequest(@PathVariable String networkName,
                                                 @RequestBody String callbackUrl) {

        Network network = new AbstractNetwork(networkName) {};
        NetworkOAuthManager oAuthManager = new FindByNetwork<>(oAuthManagers).find(network);
        OAuthAccessRequest oAuthAccessRequest = oAuthManager.createOAuthRequest(callbackUrl);
        return oAuthAccessRequest;
    }

    @RequestMapping(value = Route.API_CONNECT_NETWORK + "/oAuth",
                    method = RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public void connectOAuth(@PathVariable String networkName,
                             @RequestBody OAuthConnectRequest connectRequest,
                             HttpSession session) {

        Network network = new AbstractNetwork(networkName) {};
        NetworkConnectService connectService = new FindByNetwork<>(connectServices).find(network);
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        connectService.connectNetworkByOAuth(relevanteId, connectRequest.getOAuthRequestToken(), connectRequest.getOAuthRequestSecret(), connectRequest.getOAuthRequestVerifier());
    }

    @RequestMapping(value = Route.API_CONNECT_NETWORK + "/basicAuth",
                    method = RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public void connectBasicAuth(@PathVariable String networkName,
                                 @RequestBody BasicAuthConnectRequest connectRequest,
                                 HttpSession session) {

        Network network = new AbstractNetwork(networkName) {};
        NetworkConnectService connectService = new FindByNetwork<>(connectServices).find(network);
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        connectService.connectNetworkByBasicAuth(relevanteId, connectRequest.getUrl(), connectRequest.getUsername(), connectRequest.getPassword());
    }

    @RequestMapping(value = Route.API_DISCONNECT_NETWORK,
                    method = RequestMethod.PUT)
    public void disconnect(@PathVariable String networkName,
                           HttpSession session) {

        Network network = new AbstractNetwork(networkName) {};
        NetworkConnectService connectService = new FindByNetwork<>(connectServices).find(network);
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        connectService.disconnectNetwork(relevanteId);
    }

}