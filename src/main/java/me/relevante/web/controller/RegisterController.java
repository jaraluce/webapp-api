package me.relevante.web.controller;

import me.relevante.web.model.RegisterResult;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.RegistrationRequest;
import me.relevante.web.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;

/**
 * Created by daniel-ibanez on 11/09/16.
 */
@RestController
@RequestMapping(value = Route.API_REGISTER)
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @RequestMapping(method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> register(@RequestBody RegistrationRequest registrationRequest) {
        try {
            RegisterResult registerResult = registerService.registerAccount(registrationRequest);
            HttpStatus httpStatus = registerResult.isNewUser() ? HttpStatus.CREATED : HttpStatus.NOT_MODIFIED;
            return new ResponseEntity(registerResult.getAuthToken(), httpStatus);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>("", HttpStatus.UNAUTHORIZED);
        }
    }

}
