package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.PocketWordpressMappingJson;
import me.relevante.web.service.PocketWordpressMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.io.InvalidObjectException;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_POCKET_WORDPRESS_HUB_MAPPINGS)
public class PocketWordpressMappingController {

    @Autowired private PocketWordpressMappingService mappingService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PocketWordpressMappingJson> getAll(@PathVariable String hubId,
                                                   HttpSession session) throws InvalidObjectException {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<PocketWordpressMappingJson> mappings = mappingService.getAllMappingsByHub(relevanteId, hubId);
        return mappings;
    }

    @RequestMapping(method = RequestMethod.POST)
    public PocketWordpressMappingJson create(@PathVariable String hubId,
                                             @RequestBody PocketWordpressMappingJson mappingJson,
                                             HttpSession session) throws InvalidObjectException {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        mappingService.putMapping(relevanteId, hubId, mappingJson);
        return mappingJson;
    }

    @RequestMapping(value = "/{mappingId}",
                    method = RequestMethod.DELETE)
    public void delete(@PathVariable String hubId,
                       @PathVariable String mappingId,
                       HttpSession session) throws InvalidObjectException {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        mappingService.deleteMapping(relevanteId, hubId, mappingId);
    }

}