package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.Level1ActionJson;
import me.relevante.web.model.json.Level2ActionJson;
import me.relevante.web.model.json.Level3ActionJson;
import me.relevante.web.model.json.Level4ActionJson;
import me.relevante.web.service.ActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_ACTIONS)
public class ActionsController {

    @Autowired private ActionsService actionsService;

    @RequestMapping(value = "/level1",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> processLevel1BulkActions(@RequestBody List<Level1ActionJson> posts,
                                                 HttpSession session) {
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<String> actionsResponses = actionsService.processLevel1Actions(relevanteId, posts);
        return actionsResponses;
    }

    @RequestMapping(value = "/level2",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> processLevel2BulkActions(@RequestBody List<Level2ActionJson> commentedPosts,
                                                 HttpSession session) {
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<String> actionsResponses = actionsService.processLevel2Actions(relevanteId, commentedPosts);
        return actionsResponses;
    }

    @RequestMapping(value = "/level3",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> processLevel3BulkActions(@RequestBody List<Level3ActionJson> users,
                                                 HttpSession session) {
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<String> actionsResponses = actionsService.processLevel3Actions(relevanteId, users);
        return actionsResponses;
    }

    @RequestMapping(value = "/level4",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> processLevel4BulkActions(@RequestBody List<Level4ActionJson> users,
                                                 HttpSession session) {
        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<String> actionsResponses = actionsService.processLevel4Actions(relevanteId, users);
        return actionsResponses;
    }

}