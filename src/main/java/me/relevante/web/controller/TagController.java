package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.TagUsersJson;
import me.relevante.web.service.UserTagService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping(value = Route.API_TAGS)
public class TagController {

    @Autowired private UserTagService userTagService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONArray getTags(HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        Map<String, Integer> appearancesByNote = userTagService.getTagAppearances(relevanteId);
        JSONArray array = new JSONArray();
        for (String note : appearancesByNote.keySet()) {
            JSONObject object = new JSONObject();
            object.put("note", note);
            object.put("num", appearancesByNote.get(note));
            array.add(object);
        }
        return array;
    }

    @RequestMapping(value = "/users",
                    method = RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public void putUsersTag(@RequestBody TagUsersJson tagUsersJson,
                            HttpSession session) throws Exception {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        userTagService.saveUsersTag(relevanteId, tagUsersJson.getEncodedUserIds(), tagUsersJson.getTag());
    }


}
