package me.relevante.web.controller;

import me.relevante.web.model.Route;
import me.relevante.web.model.json.LoginRequest;
import me.relevante.web.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;

/**
 * Created by daniel-ibanez on 11/09/16.
 */
@RestController
@RequestMapping(value = Route.API_AUTH)
public class AuthController {

    @Autowired
    private AuthService authService;

    @RequestMapping(method = RequestMethod.POST,
                    produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> processAuth(@RequestBody LoginRequest login) {
        try {
            String jwtToken = authService.processAuth(login);
            return new ResponseEntity<>(jwtToken, HttpStatus.OK);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>("Auth Error", HttpStatus.UNAUTHORIZED);
        }
    }

}
