package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import me.relevante.web.model.json.WatchlistJson;
import me.relevante.web.service.WatchlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.io.InvalidObjectException;
import java.util.List;

@RestController
@RequestMapping(value = Route.API_WATCHLISTS)
public class WatchlistController {

    @Autowired private WatchlistService watchlistService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WatchlistJson> getAll(HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<WatchlistJson> watchlists = watchlistService.getAllWatchlists(relevanteId);
        return watchlists;
    }

    @RequestMapping(method = RequestMethod.GET,
                    params = {"name"},
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public WatchlistJson getByName(@RequestParam(name = "name") String watchlistName,
                                   HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        WatchlistJson watchlist = watchlistService.getWatchlistByRelevanteIdAndName(relevanteId, watchlistName);
        return watchlist;
    }


    @RequestMapping(value = "/{watchlistId}",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public WatchlistJson getById(@PathVariable String watchlistId,
                                 HttpSession session) throws InvalidObjectException {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        WatchlistJson watchlist = watchlistService.getWatchlistById(relevanteId, watchlistId);
        return watchlist;
    }

    @RequestMapping(method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WatchlistJson> create(@RequestBody WatchlistJson watchlistToCreate,
                                                HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        WatchlistJson createdWatchlist = watchlistService.createWatchlist(relevanteId, watchlistToCreate);
        return new ResponseEntity<>(createdWatchlist, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{watchlistId}",
                    method = RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@PathVariable String watchlistId,
                       @RequestBody WatchlistJson watchlistToUpdate,
                       HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        watchlistService.updateWatchlist(relevanteId, watchlistId, watchlistToUpdate);
    }

    @RequestMapping(value = "/{watchlistId}",
                    method = RequestMethod.DELETE)
    public void delete(@PathVariable String watchlistId,
                       HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        watchlistService.deleteWatchlist(relevanteId, watchlistId);
    }

}