package me.relevante.web.interceptor;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.Route;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Value("${auth.jwtSecretKey}")
    private String secretKey;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {

        if (request.getMethod().equals(HttpMethod.OPTIONS.name())) {
            return true;
        }
        String uri = request.getRequestURI();
        if (uri.startsWith(Route.API_AUTH) || uri.startsWith(Route.API_REGISTER)) {
            return true;
        }
        try {
            final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
            final String token = authHeader.substring(7); // The part after "Bearer "
            final Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
            LocalDateTime expirationDate = LocalDateTime.ofInstant(claims.getExpiration().toInstant(), ZoneOffset.UTC);
            if (LocalDateTime.now(ZoneOffset.UTC).isAfter(expirationDate)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return false;
            }
            HttpSession session = request.getSession();
            session.setAttribute(CoreSessionAttribute.RELEVANTE_ID, claims.getSubject());
        }
        catch (final Exception e) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return false;
        }
        return true;
    }

}
